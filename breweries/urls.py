from django.urls import path

from breweries.views import brewery_list, show_brewery

urlpatterns = [
    path("", brewery_list, name = "brewery_list"),
    path("<int:id>/", show_brewery, name="show_brewery"),

]
