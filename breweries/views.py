from django.shortcuts import render, get_object_or_404, redirect

from django.contrib.auth.decorators import login_required

from breweries.models import Brewery



def brewery_list(request):
    breweries = Brewery.objects.all()
    context = {
        "breweries": breweries,
    }
    return render(request, "breweries/list.html", context)

def show_brewery(request, id):
    brewery = get_object_or_404(Brewery, id=id)
    context = {
        "brewery": brewery,
    }
    return render(request, "breweries/detail.html", context)
