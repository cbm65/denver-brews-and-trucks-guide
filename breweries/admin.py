from django.contrib import admin
from breweries.models import Brewery


@admin.register(Brewery)
class BreweryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "picture",
        "is_favorite",
        "id",
        "author",
        "address",
        "city",
    )
