from django.urls import path

from trucks.views import truck_list, show_truck

urlpatterns = [

    path("", truck_list, name = "truck_list"),
    path("<int:id>/", show_truck, name="show_truck"),

]
