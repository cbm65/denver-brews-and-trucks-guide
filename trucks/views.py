from django.shortcuts import render, get_object_or_404, redirect

from django.contrib.auth.decorators import login_required

from trucks.models import Truck



def truck_list(request):
    trucks = Truck.objects.all()
    context = {
        "trucks": trucks,
    }
    return render(request, "trucks/list.html", context)


def show_truck(request, id):
    truck = get_object_or_404(Truck, id=id)
    context = {
        "truck": truck,
    }
    return render(request, "trucks/detail.html", context)
