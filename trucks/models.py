from django.db import models
from datetime import timedelta
from django.conf import settings

class Truck(models.Model):

    name = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    is_favorite = models.BooleanField(null=False)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name = "trucks",
        on_delete=models.CASCADE,
        null=True,

    )

    def __str__(self):
        return self.name
