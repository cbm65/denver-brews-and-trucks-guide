from django.contrib import admin
from trucks.models import Truck


@admin.register(Truck)
class TruckAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "picture",
        "is_favorite",
        "id",
        "author",
    )
